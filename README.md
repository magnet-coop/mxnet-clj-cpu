# MXNet CPU docker image

Docker image equipped with everything needed to run Clojure [MXNet](https://mxnet.apache.org/) CPU library.

## How to use

First make sure [docker](https://docs.docker.com/install/) is installed (Docker 17.05 or later in needed for building image).

Pre-built docker containers are available at https://hub.docker.com/u/magnetcoop/

### Start new project

Step 1. Create new project using Leiningen and move to that dir.

```sh
lein new mxnet-hello
cd mxnet-hello
```

Step 2. Add MXNet dependencies inside `project.clj`

```clojure
(defproject mxnet-hello "0.1.0-SNAPSHOT"
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.apache.clojure-mxnet/clojure-mxnet-linux-cpu "0.1.1-SNAPSHOT"]])
```

Step 3. Start REPL in docker with mounted volumes (to download dependencies just once).

```sh
docker run \
       --volume "$PWD:/home/magnet/app" \
       --volume "$HOME/.m2:/home/magnet/.m2" \
       --volume "$HOME/.m2:/root/.m2" \
	   --interactive \
	   --tty \
	   magnetcoop/mxnet-clj-cpu \
	   lein repl
```

### tips

* You don't actually need Lein on your machine. You could start by doing `docker run magnetcoop/mxnet-clj-cpu /bin/bash` and do `lein new` inside the countainer.
* If you just want to downloads dependencies, you can use `lein deps` instead of `lein run`.

## How to build

Docker 17.05 is needed to build the image. To this run

```sh
docker build --tag magnetcoop/mxnet-clj-cpu .
```

in root directory of this repo.
