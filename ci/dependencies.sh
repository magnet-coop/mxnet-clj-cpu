#!/usr/bin/env sh

set -eu

# Bash for scripts
apk add --no-cache bash

# Image for checking bash code format
docker pull jamesmstone/shfmt

# Image for linting bash code
docker pull koalaman/shellcheck-alpine

# Image for linting Dockerfile
docker pull hadolint/hadolint

# Current version of format and shellcheck
docker run --rm jamesmstone/shfmt -version
docker run --rm koalaman/shellcheck-alpine shellcheck --version
