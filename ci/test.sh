#!/usr/bin/env bash

set -eu

mkdir -p m2

docker run \
       --rm \
       --volume "$(pwd)/test:/home/magnet/app" \
       --volume "$(pwd)/m2:/home/magnet/.m2" \
       --volume "$(pwd)/m2:/root/.m2" \
       magnetcoop/mxnet-clj-cpu:latest lein run
