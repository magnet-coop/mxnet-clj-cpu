#!/usr/bin/env bash

set -eu

docker run \
       --rm \
       --volume "$(pwd):/mnt" \
       koalaman/shellcheck-alpine \
       sh -c "find /mnt -name '*.sh' | xargs shellcheck"

docker run --rm -i hadolint/hadolint <Dockerfile
