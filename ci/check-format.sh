#!/usr/bin/env bash

set -eu

DIFF="format.diff"

docker run \
       --rm \
       --volume "$(pwd)":/code \
       --workdir /code \
       jamesmstone/shfmt \
       -d -i 4 ./*.sh |
       tee "${DIFF}"

if [[ "$(wc -c ${DIFF} | cut -c1)" -gt 0 ]]; then
    exit 1
fi
