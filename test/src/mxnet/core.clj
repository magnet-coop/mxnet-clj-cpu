(ns mxnet.core
  (:require [org.apache.clojure-mxnet.ndarray :as ndarray]))

(defn -main []
  (let [a (ndarray/ones [1 5])
        b (ndarray/ones [1 5])]
    (-> (ndarray/+ a b) (ndarray/->vec))))
