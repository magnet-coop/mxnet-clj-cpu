FROM clojure:lein-2.8.1 as lein-image

FROM ubuntu:18.04

RUN set -ex; \
    apt-get update && \
    apt-get install -y --no-install-recommends gnupg=2.2.4-1ubuntu1.1 && \
    echo "deb http://ppa.launchpad.net/timsc/opencv-3.4/ubuntu bionic main" >> /etc/apt/sources.list && \
    echo "deb-src http://ppa.launchpad.net/timsc/opencv-3.4/ubuntu bionic main" >> /etc/apt/sources.list && \
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys "8A9CA30DB3C431E3" && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
        libopencv-imgcodecs3.4=3.4.1-bionic1.1 \
        libatlas3-base=3.10.3-5 \
        libcurl3=7.58.0-2ubuntu2 \
        libgomp1=8-20180414-1ubuntu2 \
        libopenblas-base=0.2.20+ds-4 \
        openjdk-8-jdk=8u171-b11-0ubuntu0.18.04.1 \
        runit=2.1.2-9.2ubuntu1 \
        wget=1.19.4-1ubuntu2.1 && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* && \
    useradd --home-dir /home/magnet --create-home --shell /bin/bash --user-group magnet

COPY --from=lein-image /usr/local/bin/lein /usr/local/bin/

COPY --from=lein-image /usr/share/java/leiningen-2.8.1-standalone.jar /usr/share/java/

COPY run-as-user.sh /usr/local/bin/

WORKDIR /home/magnet/app

ENTRYPOINT ["/usr/local/bin/run-as-user.sh"]
