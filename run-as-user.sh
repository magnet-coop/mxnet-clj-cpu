#!/usr/bin/env bash

set -eu

WORK_DIR="/home/magnet/app"

NEW_UID=$(stat -c '%u' ${WORK_DIR})
NEW_GID=$(stat -c '%g' ${WORK_DIR})

groupmod -g "$NEW_GID" -o magnet >/dev/null 2>&1
usermod -u "$NEW_UID" -o magnet >/dev/null 2>&1

exec chpst -u magnet:magnet -U magnet:magnet env HOME="/home/magnet" "$@"
